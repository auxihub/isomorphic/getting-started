# Getting Started with Isomorphic Application

This repository contains some materials to get you started in developing isomorphic application.
`node.js` is the main technology covered in this repository.

Topics included are:
1. Current State of Web Development
2. Asynchronous Javascript
3. Introducing Express to handle Requests and Responses
4. Adding Modules to Express
5. Introducing Docker Container to effectively publish web app
6. Publishing Web App to AWS (or Google Cloud)
7. Setup Continuous Integration in Travis CI
8. Introducing Svelte to build compile-time frontend application
9. Svelte as HTTP Client
10. State Management in Svelte
11. Offline First Todo List App